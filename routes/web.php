<?php

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/key', function () use ($router) {
    return str_random(32);
});
/**
 * Routes for resource notification
 */

$router->group(['prefix' => 'auth', 'middleware' => ['api']], function () use ($router) {
   $router->post('/login', 'AuthController@login');
   $router->post('/refresh', 'AuthController@refreshToken');
});

$router->group([
   'prefix' => 'auth',
   'middleware' => ['api', 'auth']
], function () use ($router) {
   $router->post('/logout', 'AuthController@logout');
});