<?php

$router->get('/', 'HomeController@index');
$router->get('/driver', 'DriverController@index');
$router->get('/driver/{id}', 'DriverController@detail');