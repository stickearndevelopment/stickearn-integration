<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <title>surat_jalan</title>
    <style type="text/css">
    </style>

</head>
<body id="surat_jalan" lang="en-US">
<div id="_idContainer000" class="Basic-Text-Frame">
    <p class="Basic-Paragraph ParaOverride-1" style="text-align: center;">
        <span style="font-size: 14pt; font-family: aril, helvetica, sans-serif;"><strong>
        <span class="CharOverride-1" style="color: #4f2d84;">Syarat &amp; Ketentuan</span></strong></span>
    </p>
    <p class="Basic-Paragraph ParaOverride-1"
       style="text-align: left; font-size: 12pt; font-family: arial, helvetica, sans-serif; line-height: 1.2em">
        Dengan ini saya telah membaca, memahami, menyetujui, mengikatkan diri dan bersedia mematuhi semua syarat dan ketentuan sebagai mitra / rekan Stickearn sebagai berikut:
    </p>
    <ol>
        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
            <span style="font-family: aril, helvetica, sans-serif;">
                <span class="CharOverride-1" style="font-size: 14pt;">
                    <span style="font-size: 12pt;">
                        Bersedia untuk tunduk dan mematuhi semua persyaratan, ketentuan, dan peraturan dalam syarat dan ketentuan ini,  termasuk yang tidak disebutkan dalam syarat dan ketentuan ini namun telah diatur dalam peraturan dan ketentuan Stickearn lainnya.
                    </span>
                </span>&nbsp;
            </span>
        </li>
        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
            <span style="font-family: aril, helvetica, sans-serif;">
                <span class="CharOverride-1" style="font-size: 14pt;">
                    <span style="font-size: 12pt;">
                        Bersedia untuk menyetujui serta melaksanakan permintaan dari Stickearn atas setiap perubahan maupun penambahan isi/konten dan bentuk iklan selama jangka waktu pemasangan iklan.
                    </span>
                </span>&nbsp;
            </span>
        </li>
        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                    <span style="font-family: aril, helvetica, sans-serif;">
                        <span class="CharOverride-1" style="font-size: 14pt;">
                            <span style="font-size: 12pt;">
                                Kewajiban dan Hak Pokok Mitra:
                            </span>
                        </span>&nbsp;
                    </span>
            <ol style="list-style-type: lower-alpha;">
                <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                            <span class="CharOverride-1"
                                  style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                <span style="font-size: 12pt;">
                                    <span class="CharOverride-1">
                                        Menjalankan seluruh peraturan dan menyelesasikan kontrak dengan durasi waktu penuh (minimal 30 hari dan dapat berlanjut sesuai permintaan pengiklan).
                                    </span>
                                </span>
                            </span>
                </li>
                <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                    <span class="CharOverride-1"
                          style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                        <span style="font-size: 12pt;">
                            <span class="CharOverride-1">
                                Menggunakan aplikasi sesuai dengan petunjuk dan aturan yang telah ditetapkan, dan tidak memodifikasi tanpa izin resmi pihak Stickearn.
                            </span>
                        </span>
                    </span>
                </li>
                <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                    <span class="CharOverride-1"
                          style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                        <span style="font-size: 12pt;">
                            <span class="CharOverride-1">
                                Melakukan Unduh/update aplikasi Stickearn hanya melalui PlayStore.
                            </span>
                        </span>
                    </span>
                </li>
                <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                    <span class="CharOverride-1"
                          style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                        <span style="font-size: 12pt;">
                            <span class="CharOverride-1">
                                Melakukan check-in berkala dan mengaktifkan aplikasi StickEarn setiap kali berkendara.
                            </span>
                        </span>
                    </span>
                </li>
                <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                    <span class="CharOverride-1"
                          style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                        <span style="font-size: 12pt;">
                            <span class="CharOverride-1">
                                Melakukan pencapain pemenuhan jarak tempuh sebesar 3000km/bulan untuk area JABODETABEK dan 1000-2000km/bulan untuk area diluar JABODETABEK atau sesuai peraturan yang ditetapkan di area campaign tersebut.
                            </span>
                        </span>
                    </span>
                    <ul>
                        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                            <span class="CharOverride-1"
                                  style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                <span style="font-size: 12pt;">
                                    <span class="CharOverride-1">
                                        Acuan perhitungan jarak tempuh (ODO-meter) adalah Kilometer Evaluasi.
                                    </span>
                                </span>
                            </span>
                        </li>
                    </ul>
                </li>
                <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                            <span class="CharOverride-1"
                                  style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                <span style="font-size: 12pt;">
                                    <span class="CharOverride-1">
                                        Melakukan pelaporan dengan mengunggah/upload foto evaluasi secara berkala setiap minggunya sesuai jadwal yang tertera pada aplikasi Stickearn.
                                    </span>
                                </span>
                            </span>
                    <ul>
                        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                                    <span class="CharOverride-1"
                                          style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                        <span style="font-size: 12pt;">
                                            <span class="CharOverride-1">
                                                Foto evaluasi meluputi, ODO-meter (bukan Trip A/B) dan semua sisi bodi mobil yang terdapat iklan.
                                            </span>
                                        </span>
                                    </span>
                        </li>
                    </ul>
                </li>
                <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                            <span class="CharOverride-1"
                                  style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                <span style="font-size: 12pt;">
                                    <span class="CharOverride-1">
                                        Menjaga kondisi iklan yang terpasang dengan sebaik-baiknya dan tidak memodifikasi iklan.
                                    </span>
                                </span>
                            </span>
                </li>
                <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                            <span class="CharOverride-1"
                                  style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                <span style="font-size: 12pt;">
                                    <span class="CharOverride-1">
                                        Mendapatkan pembayaran dari terpenuhinya durasi kontrak, tercapainya jarak tempuh, dan terselesaikannya peraturan yang telah disepakati tanpa pelanggaran.
                                    </span>
                                </span>
                            </span>
                </li>
                <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                            <span class="CharOverride-1"
                                  style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                <span style="font-size: 12pt;">
                                    <span class="CharOverride-1">
                                        Mendapatkan perbaikan iklan (aktif) yang terpasang apabila ditemukan perubahan /atau perbedaan dari design awal yang disebabkan bukan karna pelangaran.
                                    </span>
                                </span>
                            </span>
                </li>
                <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                            <span class="CharOverride-1"
                                  style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                <span style="font-size: 12pt;">
                                    <span class="CharOverride-1">
                                        Mendapatkan perbaikan iklan yang sedang terpasang apabila ada perubahan /atau perbedaan design yang disebabkan bukan karena pelangaran, dengan tidak ada pembayaran tambahan untuk Mitra.
                                    </span>
                                </span>
                            </span>
                </li>

            </ol>
        </li>
        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                    <span style="font-family: aril, helvetica, sans-serif;">
                        <span class="CharOverride-1" style="font-size: 14pt;">
                            <span style="font-size: 12pt;">
                                Sanksi:
                            </span>
                        </span>&nbsp;
                    </span>
            <ol style="list-style-type: lower-alpha;">
                <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                            <span class="CharOverride-1"
                                  style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                <span style="font-size: 12pt;">
                                    <span class="CharOverride-1">
                                        Apabila Mitra melakukan pembatalan iklan (cancel campaign) sebelum selesainya jangka waktu pemasangan iklan, maka Mitra akan di blacklist secara permanen dan dikenakan penalty sebesar 50% dari total pembayaran.
                                    </span>
                                </span>
                            </span>
                </li>
                <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                            <span class="CharOverride-1"
                                  style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                <span style="font-size: 12pt;">
                                    <span class="CharOverride-1">
                                        Apabila Mitra tidak melakukan check-in berkala dan mengaktifkan aplikasi Stickearn setiap kali berkendara, atau dalam jangka waktu minimal 20 hari/bulan, maka diberlakukan pengurangan pembayaran sebesar Rp.20.000,-per hari.
                                    </span>
                                </span>
                            </span>
                </li>
                <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                    <span class="CharOverride-1"
                          style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                        <span style="font-size: 12pt;">
                            <span class="CharOverride-1">
                                Apabila Mitra tidak melakukan pelaporan dengan mengunggah/upload foto evaluasi secara berkala setiap per-minggu sekali melalui aplikasi Stickearn, maka diberlakukan pengurangan pembayaran sebesar Rp.40.000,-per minggu.
                            </span>
                        </span>
                    </span>
                </li>
                <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                    <span class="CharOverride-1"
                          style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                        <span style="font-size: 12pt;">
                            <span class="CharOverride-1">
                                Selama iklan berjalan, Stickearn berhak membatalkan pemasangan iklan serta menggugurkan pembayaran secara sepihak bila ditemukan pelanggaran dan/atau kecurangan yang termasuk namun tidak terbatas pada:
                            </span>
                        </span>
                    </span>
                    <ul>
                        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                            <span class="CharOverride-1"
                                  style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                <span style="font-size: 12pt;">
                                    <span class="CharOverride-1">
                                        Mitra terkena Suspended oleh Pihak Grab
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                            <span class="CharOverride-1"
                                  style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                <span style="font-size: 12pt;">
                                    <span class="CharOverride-1">
                                        Mitra memodifikasi aplikasi tanpa izin resmi pihak StickEarn.
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                            <span class="CharOverride-1"
                                  style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                <span style="font-size: 12pt;">
                                    <span class="CharOverride-1">
                                        Mitra memodifikasi iklan (aktif) terpasang tanpa izin resmi pihak StickEarn.
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                            <span class="CharOverride-1"
                                  style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                <span style="font-size: 12pt;">
                                    <span class="CharOverride-1">
                                        Mitra memberikan pelaporan evaluasi palsu.
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                            <span class="CharOverride-1"
                                  style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                <span style="font-size: 12pt;">
                                    <span class="CharOverride-1">
                                        Mitra tidak bersedia untuk memenuhi permintaan dari Stickearn untuk merubah maupun menambah isi/konten dan bentuk iklan selama jangka waktu pemasangan iklan.
                                    </span>
                                </span>
                            </span>
                        </li>
                        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                            <span class="CharOverride-1"
                                  style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                                <span style="font-size: 12pt;">
                                    <span class="CharOverride-1">
                                        Dan segala bentuk pelanggaran dan/atau kecurangan terhadap isi syarat dan ketentuan ini maupun peraturan dan ketentuan Stickearn lainnya.
                                    </span>
                                </span>
                            </span>
                        </li>
                    </ul>
                </li>
                <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
                    <span class="CharOverride-1"
                          style="font-size: 14pt; font-family: aril, helvetica, sans-serif;">
                        <span style="font-size: 12pt;">
                            <span class="CharOverride-1">
                                Pihak Stickearn berhak tidak melanjutkan kerja sama dengan mitra berdasarkan pertinjauan dari pihak Stickearn.
                            </span>
                        </span>
                    </span>
                </li>
            </ol>
        </li>
        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
            <span style="font-family: aril, helvetica, sans-serif;">
                <span class="CharOverride-1" style="font-size: 14pt;">
                    <span style="font-size: 12pt;">
                        Terkait masalah teknis yang timbul dari pemasangan dan pencopotan iklan pada mobil mitra yang telah sesuai dengan standar, pihak StickEarn dibebaskan dari tuntutan dan tanggung jawab apapun.
                    </span>
                </span>
            </span>
        </li>
        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
            <span style="font-family: aril, helvetica, sans-serif;">
                <span class="CharOverride-1" style="font-size: 14pt;">
                    <span style="font-size: 12pt;">
                        Segala bentuk keluhan, kritik dan saran dari mitra disampaikan dalam bentuk surat dan/atau formulir khusus dan ditanda tangani oleh mitra dan akan diteruskan dengan secara bertahap.
                    </span>
                </span>
            </span>
        </li>
        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
            <span style="font-family: aril, helvetica, sans-serif;">
                <span class="CharOverride-1" style="font-size: 14pt;">
                    <span style="font-size: 12pt;">
                        Segala bentuk tindakan kriminal dan kekerasan serta ancaman melanggar hukum yang ditujukan kepada pihak StickEarn, akan diselesaikan dan diproses sesuai hukum yang berlaku.
                    </span>
                </span>
            </span>
        </li>
        <li class="Basic-Paragraph ParaOverride-1" style="text-align: left;">
            <span style="font-family: aril, helvetica, sans-serif;">
                <span class="CharOverride-1" style="font-size: 14pt;">
                    <span style="font-size: 12pt;">
                        Stickearn dibebaskan dari seluruh tuntutan dan tanggung jawab dari pihak lainnya atas semua pelanggaran, perbuatan melawan hukum, kelalaian, kecurangan, atau hal lainnya yang disebabkan oleh Mitra dalam rangka kerjasama ini.
                    </span>
                </span>
            </span>
        </li>
    </ol>
</div>
</body>
</html>