<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8" />
		<title>surat_jalan</title>
		<style type="text/css">

		</style>

	</head>
	<body id="surat_jalan" lang="en-US">
		<div id="_idContainer000" class="Basic-Text-Frame">
<p class="Basic-Paragraph ParaOverride-1" style="text-align: left;"><span style="font-size: 14pt; font-family: aril, helvetica, sans-serif;"><strong><span class="CharOverride-1" style="color: #4f2d84;">Surat Jalan</span></strong></span></p>
<p class="Basic-Paragraph ParaOverride-1" style="text-align: left;"><span style="font-size: 14pt; font-family: aril, helvetica, sans-serif;"><strong><span class="CharOverride-1" style="color: #4f2d84;">[number]</span></strong></span></p>
<p class="Basic-Paragraph ParaOverride-1" style="text-align: left;"><span class="CharOverride-1" style="font-family: aril, helvetica, sans-serif;">Pada hari ini, [DD], tanggal [dd MMMM yyyy],<br />Kami yang bertanda tangan dibawah ini:</span></p>
<p class="Basic-Paragraph ParaOverride-1" style="text-align: left;"><span class="CharOverride-1" style="font-family: aril, helvetica, sans-serif;">
	<table>
		<tr><td>I.</td><td>Nama</td><td>:</td><td>PT. Paragon Pratama Teknologi</td></tr>
		<tr><td></td><td>Alamat</td><td>:</td><td>Jl.Letnan Suprapto 400</td></tr>
		<tr><td></td><td></td><td>:</td><td>Cempaka Putih </td></tr>
		<tr><td></td><td></td><td>:</td><td>Jakarta Pusat - 10510</td></tr>
		<tr><td></td><td>Telepon</td><td>:</td><td>(021) 27899699</td></tr>
	</table>
<p class="Basic-Paragraph ParaOverride-1" style="text-align: left;"><span class="CharOverride-1" style="font-family: aril, helvetica, sans-serif;">Dalam hal ini selanjutnya disebut PIHAK PERTAMA.</span></p>
<table>
	<tr><td>II.</td><td>Nama</td><td>:</td><td>[Nama]</td></tr>
	<tr><td></td><td>Nomor KTP </td><td>:</td><td>[KTP]</td></tr>
	<tr><td></td><td>Jenis Kendaraan</td><td>:</td><td>[Model] [Tipe]</td></tr>
	<tr><td></td><td>Nomor Polisi</td><td>:</td><td>[Nomor Polisi]</td></tr>

</table>
<p class="Basic-Paragraph ParaOverride-1" style="text-align: left;"><span class="CharOverride-1" style="font-family: aril, helvetica, sans-serif;">Dalam hal ini bertindak berdasarkan atas nama Pribadi<br />selanjutnya disebut PIHAK KEDUA.</span></p>
<p class="Basic-Paragraph ParaOverride-1" style="text-align: left;"><span class="CharOverride-1" style="font-family: aril, helvetica, sans-serif;">PIHAK PERTAMA memberikan surat jalan kepada PIHAK KEDUA untuk membrading mobil dengan iklan [Iklan],<br />Selama :</span></p>
<p class="Basic-Paragraph ParaOverride-1" style="text-align: left;"><span class="CharOverride-1" style="font-family: aril, helvetica, sans-serif;">[start_contract_date] - [end_contract_date]</span></p>
<p class="Basic-Paragraph ParaOverride-1" style="text-align: left;"><span class="CharOverride-1" style="font-family: aril, helvetica, sans-serif;">Kami yang bertanda tangan dibawah ini</span></p>
<p class="Basic-Paragraph ParaOverride-1" style="text-align: left;"><span class="CharOverride-1" style="font-family: aril, helvetica, sans-serif;">PIHAK PERTAMA<br />PT Paragon Pratama Teknologi</span></p>
<p class="Basic-Paragraph ParaOverride-1" style="text-align: left;"><span class="CharOverride-1" style="font-family: aril, helvetica, sans-serif;">TTd</span></p>
</div>
	</body>
</html>
