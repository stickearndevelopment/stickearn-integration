<?php

namespace App\Models\Stickearn\V1;

use Illuminate\Database\Eloquent\Model;

class VehicleClass extends Model
{
    protected $table = 'stickearn_v2.vehicle_class';
    protected $fillable = ['name'];
    public $timestamps = false;
}
