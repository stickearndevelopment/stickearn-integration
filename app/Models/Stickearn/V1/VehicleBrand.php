<?php

namespace App\Models\Stickearn\V1;

use Illuminate\Database\Eloquent\Model;

class VehicleBrand extends Model
{
    protected $table = 'stickearn_v2.vehicle_brand';
    protected $fillable = ['name','vehicle_class_id'];
    public $timestamps = false;
    
    public function vehicle_class()
    {
    	return $this->belongsTo(VehicleClass::class, 'vehicle_class_id');
    }
}
