<?php

namespace App\Models\Stickearn\V1;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;

class Campaign extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';

    protected $fillable = ['end_date'];
    protected $table = 'campaign';
    protected $dates = ['start_date', 'end_date', 'created_date', 'updated_date'];
    protected $with = [];

    protected $appends = [
        'wrap_type_id'
    ];

    //relations
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function activeTimes()
    {
        return $this->hasMany(CampaignActiveTime::class, 'campaign_id');
    }
    
    public function city()
    {
        return $this->belongsTo(CampaignCity::class, 'city_id');
    }
    
    public function prices()
    {
        return $this->hasMany(CampaignPrice::class, 'campaign_id');
    }

    public function travelledAreas()
    {
        return $this->hasMany(CampaignTravelledArea::class, 'campaign_id');
    }

    public function stats()
    {
    	return $this->hasOne(CampaignStats::class, 'campaign_id');
    }
    
    public function callcenters()
    {
        return $this->belongsToMany(Admin::class, 'stick_earn.campaign_callcenter', 'campaign_id', 'user_id');
    }

    public function drivers()
    {
    	return $this->belongsToMany(Driver::class, 'campaign_request', 'campaign_id', 'driver_id')
                ->whereIn('campaign_request.status', ['approved','completed','suspended']);
//                ->whereIn('campaign_request.id',function($query){
//                    $query->selectRaw('max(id)')->from('campaign_request')->groupBy('campaign_id','driver_id');
//                })
//                ->where('user_profile.is_active','=','1');
    }
            
    public function driversNotReplaced()
    {
    	return $this->belongsToMany(Driver::class, 'campaign_request', 'campaign_id', 'driver_id')
                ->whereIn('campaign_request.status', ['approved','completed','suspended'])
//                ->whereIn('campaign_request.id',function($query){
//                    $query->selectRaw('max(id)')->from('campaign_request')->groupBy('campaign_id','driver_id');
//                })
//                ->where('user_profile.is_active','=','1')
                ->whereNull('campaign_request.replacer');
    }
    
    public function driversReplaced()
    {
    	return $this->belongsToMany(Driver::class, 'campaign_request', 'campaign_id', 'driver_id')
                ->whereIn('campaign_request.status', ['approved','completed','suspended'])
                ->whereIn('campaign_request.id',function($query){
                    $query->selectRaw('max(id)')->from('campaign_request')->groupBy('campaign_id','driver_id');
                })
//                ->where('user_profile.is_active','=','1')
                ->whereNotNull('campaign_request.replacer')
                ->orderBy('id','desc');
    }
    
    public function driversActive()
    {
    	return $this->belongsToMany(Driver::class, 'campaign_request', 'campaign_id', 'driver_id')
                ->whereIn('campaign_request.status', ['approved','completed']);
//                ->whereIn('campaign_request.id',function($query){
//                    $query->selectRaw('max(id)')->from('campaign_request')->groupBy('campaign_id','driver_id');
//                })
//                ->where('user_profile.is_active','=','1');
    }
    
    public function driversAll()
    {
    	return $this->belongsToMany(Driver::class, 'campaign_request', 'campaign_id', 'driver_id')
//                ->where('user_profile.is_active','=','1')
//                ->whereIn('campaign_request.id',function($query){
//                    $query->selectRaw('max(id)')->from('campaign_request')->groupBy('campaign_id','driver_id');
//                })
                    ;
    }
    
    public function campaignRequests()
    {
        return $this->hasMany(CampaignRequest::class, 'campaign_id')->whereIn('campaign_request.status', ['approved','completed','suspended']);
    }
    public function campaignRequestsCurrent()
    {
        return $this->hasMany(CampaignRequest::class, 'campaign_id')->whereIn('campaign_request.status', ['approved']);
    }
    public function campaignrequestsAll()
    {
    	return $this->hasMany(CampaignRequest::class, 'campaign_id');
    }

    public function areas()
    {
        return $this->hasMany(CampaignArea::class);
    }
    
    public function addons()
    {
        return $this->belongsToMany(Addon::class, 'stickearn_v2.campaign_addon', 'campaign_id', 'addon_id');
    }
    
    public function driversCountRelation()
    {
        return $this->belongsToMany(Driver::class, 'campaign_request', 'campaign_id', 'driver_id')
                ->selectRaw('count(campaign_request.driver_id) as aggregate')
                ->whereIn('campaign_request.status', ['approved','completed','suspended'])
//                ->where('user_profile.is_active','=','1')
                ->whereIn('campaign_request.id',function($query){
                    $query->selectRaw('max(id)')->from('campaign_request')->groupBy('campaign_id','driver_id');
                })
                ->groupBy('campaign_id');
    }
    
    public function driversActiveCountRelation()
    {
        return $this->belongsToMany(Driver::class, 'campaign_request', 'campaign_id', 'driver_id')
                ->selectRaw('count(campaign_request.driver_id) as aggregate')
                ->whereIn('campaign_request.status', ['approved','completed'])
//                ->where('user_profile.is_active','=','1')
                ->whereIn('campaign_request.id',function($query){
                    $query->selectRaw('max(id)')->from('campaign_request')->groupBy('campaign_id','driver_id');
                })
                ->groupBy('campaign_id');
    }
    
   
    //mutators
    public function setSetCodeAttribute($value){ 
        $this->campaign_code = $value;
        $this->save();
        
        $code = DB::table('code')->where('code','=',$value)->first();
        if(empty($code)){
            DB::table('code')->insert(['code'=>$value]);    
        }
    }
    
    public function setSetStatusAttribute($value){ 
        $this->status = $value;
        $this->save();
        
        if($value == 'pending' || $value == 'approved' || $value == 'wrapped'){
            DB::table('campaign_request')->where('campaign_id','=',$this->id)->whereNotIn('status',['suspended','rejected'])->update(['status'=>'approved']);
        }else if($value == 'active'){
            DB::table('campaign_request')->where('campaign_id','=',$this->id)->whereNotIn('status',['suspended','rejected'])->update(['status'=>'approved']);
        }else if($value == 'completed'){
            DB::table('campaign_request')->where('campaign_id','=',$this->id)->whereNotIn('status',['suspended','rejected'])->update(['status'=>'completed']);
        }else if($value == 'rejected'){
            //rejected
        }
    }
    
    public function setExtendAttribute($month){
        $this->duration += $month;
        $prev_end_date = $this->end_date;
        if($this->is_thirty_days > 0){
            $this->end_date = $this->end_date->addDays($month*30);
        }else{
            $this->end_date = $this->end_date->addMonth($month);
        }
        $this->save();
        
        //extend evaluation
        if($this->persistent_end_date == 0){
            if($this->is_thirty_days > 0){
                foreach($this->drivers as $driver){
                    
                    $cr = CampaignRequest::where('campaign_id','=',$this->id)->where('driver_id','=',$driver->id)->orderBy('id','desc')->first();
                    $ev = Evaluation::where('campaign_id','=',$this->id)->where('driver_id','=',$driver->id)->where('start_date','!=',$prev_end_date)->orderBy('eval_num','desc')->first();

                    //create eval inbetween
                    $end_date = $this->end_date;
                    $start_date = date('Y-m-d', strtotime($ev->start_date . " +6 days"));
                    $date_now = $start_date;
                    $date_i = $start_date;
                    $eval_num_i = $ev->eval_num+1;
                    while($date_i < $end_date){
                        if($date_i > $date_now){
                            $cr->add_evaluation = [
                                'start_date'=>date('Y-m-d',strtotime($date_i)).' 00:00:00',
                                'end_date'=>date('Y-m-d',strtotime($date_i)).' 23:59:59',
                                'eval_num'=>$eval_num_i
                            ];
                        }
                        $date_i = date('Y-m-d', strtotime($date_i . " +".($eval_num_i == 1 ? '5' : '6')." days"));
                        $eval_num_i++;
                    }

                    //create last eval1
                    if($this->must_remove > 0){
                        $cr->add_evaluation = [
                            'start_date'=>date('Y-m-d',strtotime($end_date)).' 00:00:00',
                            'end_date'=>date('Y-m-d',strtotime($end_date)).' 23:59:59',
                            'eval_num'=>$eval_num_i
                        ];
                    }
                    
                }
            }else{
                DB::statement( "call stickearn_v2.sp_create_evaluation_3_extend_all(:campaign_id);", ['campaign_id'=>$this->id] );
            }
        }else{
            //UNFINISHED
        }
    }
    
    public function setCustomDateAttribute($val){
        if($val==0){
            $this->persistent_end_date = 0;
            $this->save();
        }else if($val==1){
            $this->persistent_end_date = 1;
            $this->save();
            
            $last_date = DB::table('campaign_request')->where('campaign_id','=',$this->id)->orderBy('created_date','desc')->first();
            if(!empty($last_date)){
                $this->end_date = $last_date->created_date;
            }
            
//            //extend evaluation
//            if($this->persistent_end_date == 0){
//                if($this->is_thirty_days > 0){
//                    foreach($this->drivers as $driver){
//
//                        $cr = CampaignRequest::where('campaign_id','=',$campaign_id)->where('driver_id','=',$driver_id)->orderBy('id','desc')->first();
//                        $ev = Evaluation::where('campaign_id','=',$campaign_id)->where('driver_id','=',$driver_id)->where('start_date','!=',$prev_end_date)->orderBy('eval_num','desc')->first();
//
//                        //create eval inbetween
//                        $end_date = $this->end_date;
//                        $start_date = date('Y-m-d', strtotime($ev->start_date . " +6 days"));
//                        $date_now = $start_date;
//                        $date_i = $start_date;
//                        $eval_num_i = $ev->eval_num+1;
//                        while($date_i < $end_date){
//                            if($date_i > $date_now){
//                                $cr->add_evaluation = [
//                                    'start_date'=>date('Y-m-d',strtotime($date_i)).' 00:00:00',
//                                    'end_date'=>date('Y-m-d',strtotime($date_i)).' 23:59:59',
//                                    'eval_num'=>$eval_num_i
//                                ];
//                            }
//                            $date_i = date('Y-m-d', strtotime($date_i . " +".($eval_num_i == 1 ? '5' : '6')." days"));
//                            $eval_num_i++;
//                        }
//
//                        //create last eval1
//                        if($campaign->must_remove > 0){
//                            $cr->add_evaluation = [
//                                'start_date'=>date('Y-m-d',strtotime($end_date)).' 00:00:00',
//                                'end_date'=>date('Y-m-d',strtotime($end_date)).' 23:59:59',
//                                'eval_num'=>$eval_num_i
//                            ];
//                        }
//
//                    }
//                }else{
//                    DB::statement( "call sp_create_evaluation_3_extend_all(:campaign_id);", ['campaign_id'=>$this->id] );
//                }
//            }else{
//                //UNFINISHED
//            }
        }
    }
    
    //helper properties
    public function getDistanceAttribute(){ return empty($this->stats) ? 0 : $this->stats->distance; }
    public function getOuterDistanceAttribute(){ return empty($this->stats) ? 0 : $this->stats->outer_distance; }
    public function getImpressionAttribute(){ return empty($this->stats) ? 0 : $this->stats->impression; }        
    
    public function getDriversCountAttribute(){
        if ( ! array_key_exists('driversCountRelation', $this->relations)) $this->load('driversCountRelation');
        $related = $this->getRelation('driversCountRelation')->first();
        return ($related) ? $related->aggregate : 0;
    }
    
    public function getDriversActiveCountAttribute(){
        if ( ! array_key_exists('driversActiveCountRelation', $this->relations)) $this->load('driversActiveCountRelation');
        $related = $this->getRelation('driversActiveCountRelation')->first();
        return ($related) ? $related->aggregate : 0;
    }
    
    public function getDriversSuspendedCountAttribute(){
        if ( ! array_key_exists('driversActiveCountRelation', $this->relations)) $this->load('driversActiveCountRelation');
        $related = $this->getRelation('driversActiveCountRelation')->first();
        return ($related) ? $related->aggregate : 0;
    }
    
    public function getDriversReplacementCountAttribute(){
        if ( ! array_key_exists('driversActiveCountRelation', $this->relations)) $this->load('driversActiveCountRelation');
        $related = $this->getRelation('driversActiveCountRelation')->first();
        return ($related) ? $related->aggregate : 0;
    }
    
    public function getDaysLeftAttribute()
    {
    	$today = Carbon::now();
    	return $this->end_date->diffInDays($today);
    }
    
    public function getDaysPastAttribute()
    {
    	$today = Carbon::now();
        if($this->start_date->diffInDays($this->end_date) < $this->start_date->diffInDays($today)){
            return $this->start_date->diffInDays($this->end_date);
        }else{
            return $this->start_date->diffInDays($today);
        }
    }
    
    public function getTotalPriceAttribute()
    {
    	return $this->prices()->avg('advert_price') * $this->duration * $this->drivers_count;
    }
    
    public function getCpiAttribute(){
        return $this->impression < 1 ? 0 : (($this->days_past / 30) * ($this->total_price / $this->impression));
    }
    
    public function getDurationAttribute(){
        return $this->getOriginal('duration') + 0;
    }
    
    
    //display properties
    public function getDisplayDistanceAttribute(){ return thousandSeparator($this->distance, 0).' Km' ; }
    public function getDisplayOuterDistanceAttribute(){ return thousandSeparator($this->distance, 0).' Km'; }
    public function getDisplayImpressionAttribute(){ return thousandSeparator($this->impression, 0); }
    public function getDisplayCpiAttribute(){ return 'Rp. '.number_format($this->cpi, 2, '.', ''); }
    
    public function getDriverCountStringAttribute(){
        $cars = thousandSeparator($this->car_per_area);
        $string = "$this->drivers_count / $cars cars";
        return $string;
    }
    
    public function getEndDateadjustedAttribute(){
        return strtotime('-1 day',strtotime($this->end_date->format('Y-m-d')));
    }
    
    public function getLastDateAttribute(){
        $end_date = strtotime('-1 day',strtotime($this->end_date->format('Y-m-d')));
        return $end_date > strtotime('now') ? strtotime('now') : $end_date;
    }
    
    public function getPeriodStringAttribute(){
        $start_date = $this->start_date->format(config('appconfig.date_format'));
        $end_date = date(config('appconfig.date_format'),strtotime('-1 day',strtotime($this->end_date->format('Y-m-d'))));
        $string = "$this->duration months ( $start_date - $end_date )";
        return $string;
    }
    
    public function getStartEndDateStringAttribute(){
        $start_date = $this->start_date->format(config('appconfig.date_format'));
        $end_date = date(config('appconfig.date_format'),strtotime('-1 day',strtotime($this->end_date->format('Y-m-d'))));
        $string = "$start_date - $end_date";
        return $string;
    }
    
    public function getCityStringAttribute(){
        if ( ! array_key_exists('city', $this->relations)) $this->load('city');
        if(empty($this->city)){
            return "Undefined";
        }
        return $this->city->name;
    }
    
    public function getCityTargetStringAttribute(){
        if ( ! array_key_exists('city', $this->relations)) $this->load('city');
        if(empty($this->city)){
            return "Undefined";
        }
        
        
        return $this->city->name.' ('.$this->city->target*$this->duration.' Km)';
    }
    
//    public function getWrapTypeStringAttribute() {
//        if(!empty($this->prices[0])){
//            return $this->prices[0]->wrap->factor_name;
//        }
//        return '';
//    }
//    
//    public function getWrapTypeIdAttribute() {
//        if(!empty($this->prices[0])){
//            return $this->prices[0]->wrap->id;
//        }
//        return '';
//    }
    
    public function getWrapTypeStringAttribute() {
        $string = '';
        if (is_numeric($this->wrap_type)) {
            foreach (config('appconfig.wraps') as $wrap) {
                if ($wrap['id'] == $this->wrap_type){
                    $string = $wrap['text'];
                }
            }
        } else {
            foreach (config('appconfig.wraps') as $index => $wrap) {
                if (!empty(explode(',', $this->wrap_type)[$index]) && explode(',', $this->wrap_type)[$index] !== 'false'){
                    $string = $wrap['text'];
                }
            }
        }
        return $string;
    }
    
    public function getWrapTypeIdAttribute() {
        $string = 1;
        if (is_numeric($this->wrap_type)) {
            foreach (config('appconfig.wraps') as $wrap) {
                if ($wrap['id'] == $this->wrap_type){
                    $string = $wrap['id'];
                }
            }
        } else {
            foreach (explode(',', $this->wrap_type) as $int) {
                if ($int !== 'false'){
                    foreach (config('appconfig.wraps') as $wrap) {
                        if ($wrap['id'] == $int){
                            $string = $wrap['id'];
                        }
                    }
                }
            }
        }
        return $string;
    }
    
    public function getEvalCountAttribute(){
        return 5 * $this->duration + 1;
    }
    
    public function setGenerateEvaluationsAttribute($value){
        $this->load('campaignRequestsCurrent.driver');
        foreach($this->campaignRequestsCurrent as $cr){
            $cr->generate_evaluations = true;
        }
    }

}
