<?php

namespace App\Models\Stickearn\V1;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';

    protected $table = 'vehicle';
    protected $with = [];
    
    public function vehicle_type()
    {
    	return $this->belongsTo(VehicleType::class, 'vehicle_type_id');
    }
    
    public function vehicle_color()
    {
    	return $this->belongsTo(VehicleColor::class, 'vehicle_color_id');
    }
    
    public function driver()
    {
    	return $this->belongsTo(Driver::class, 'fk_user_id');
    }
    
    //helper properties
    public function getTypeAttribute(){ 
        return empty($this->vehicle_type) ? $this->model : $this->vehicle_type->name; 
    }
    
    public function getColorAttribute(){ 
        return empty($this->vehicle_color) ? $this->getOriginal('color') : $this->vehicle_color->name; 
    }
    
    public function getBrandAttribute(){ 
        return empty($this->vehicle_type) ? $this->brand_name : $this->vehicle_type->vehicle_brand->name; 
    }
    
    //inventory
    public function transactions(){
        return $this->hasMany(Inventory\AssetTransaction::class, 'vehicle_id');
    }
    public function getLastAssetEvaluationAttribute(){ //MUST EAGER LOAD transactions.lists.last_evaluation.campaign_evaluation
        $eval = [];
        foreach($this->last_asset_evaluations as $e){
            if(!@$eval || $eval->computed_start_date > $e->computed_start_date){
                $eval = $e;
            }
        }
        return $eval;
    }
    public function getLastAssetEvaluationNullAttribute(){ //MUST EAGER LOAD transactions.lists.last_evaluation.campaign_evaluation. if have list with no eval, return null
        $eval = [];
        foreach($this->last_asset_evaluations as $e){
            if(!@$e){
                return $e;
            }
            if(!@$eval || $eval->computed_start_date > $e->computed_start_date){
                $eval = $e;
            }
        }
        return $eval;
    }
    public function getLastAssetEvaluationsAttribute(){ //MUST EAGER LOAD transactions.lists.last_evaluation.campaign_evaluation
        $evals = [];
        foreach($this->transactions as $trans){
            foreach($trans->lists as $list){
                $evals[] = $list->last_evaluation;
            }
        }
        return $evals;
    }
    public function getNextAssetEvaluationDateAttribute(){ //MUST EAGER LOAD transactions.lists.last_evaluation.campaign_evaluation, transactions.lists.asset.category
        //get lowest assets' eval interval
        if(!@$this->transactions) return null;
        
        $interval = 0;
        foreach($this->transactions as $trans){
            foreach($trans->lists as $list){
                if(!@$list->return_date && ($interval == 0 || ($list->asset->category->require_eval > 0 && $interval > $list->asset->category->require_eval))){
                    $interval = $list->asset->category->require_eval;
                }
            }
        }
        
        //get last date
        $eval = [];
        foreach($this->transactions as $trans){
            foreach($trans->lists as $list){
                if(!@$list->return_date && (!@$eval || $eval->computed_start_date > $list->last_evaluation->computed_start_date)){
                    $eval = $list->last_evaluation;
                }
            }
        }
        
        if(!@$eval->computed_start_date) return null;
        return date('Y-m-d H:i:s',strtotime('+'.$interval.' days', strtotime($eval->computed_start_date)));
    }
}
