<?php

namespace App\Models\Stickearn\V1;

use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    protected $table = 'stickearn_v2.vehicle_type';
    protected $with = ['vehicle_brand','vehicle_brand.vehicle_class'];
    protected $fillable = ['name','vehicle_brand_id'];
    public $timestamps = false;
    
    public function vehicle_brand()
    {
    	return $this->belongsTo(VehicleBrand::class, 'vehicle_brand_id');
    }
}
