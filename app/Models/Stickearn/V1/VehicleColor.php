<?php

namespace App\Models\Stickearn\V1;

use Illuminate\Database\Eloquent\Model;

class VehicleColor extends Model
{
    protected $table = 'stickearn_v2.vehicle_color';
    protected $fillable = ['name', 'hex'];
    public $timestamps = false;
}
