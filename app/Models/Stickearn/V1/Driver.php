<?php

namespace App\Models\Stickearn\V1;

use Illuminate\Database\Eloquent\Model;
use App\Events;
use Auth;

class Driver extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';

    protected $table = 'user_profile';
    protected $guarded = [];
    protected $hidden = ['password'];
    protected $with = ['vehicle'];

    public function campaigns()
    {
    	return $this->belongsToMany(Campaign::class, 'campaign_request', 'driver_id', 'campaign_id')->whereIn('campaign_request.status', ['approved','completed','suspended'])->orderBy('start_date','desc');
    }
    
    public function campaign_requests()
    {
    	return $this->hasMany(CampaignRequest::class,'driver_id');
    }
    
    public function campaignsAll()
    {
    	return $this->belongsToMany(Campaign::class, 'campaign_request', 'driver_id', 'campaign_id');
    }
    
    public function latest_campaign_requests()
    {
        return $this->hasMany(CampaignRequest::class,'driver_id')->with('campaign')->whereIn('campaign_request.status',['approved','completed','suspended'])->latest('campaign_request.updated_date');
    }
    
    public function stats()
    {
    	return $this->hasOne(DriverDistance::class, 'driver_id')->selectRaw('driver_id, sum(distance) as distance, sum(impression) as impression, count(case when distance>0 then 1 else 0 end) as durasi')->groupBy('driver_id');
    }
    
    public function stats_raw()
    {
    	return $this->hasOne(DriverDistanceRaw::class, 'driver_id')->selectRaw('driver_id, sum(distance) as distance, sum(impression) as impression, count(case when distance>0 then 1 else 0 end) as durasi')->groupBy('driver_id');
    }
    
    public function coordinate_count()
    {
    	return $this->hasOne(CoordinateCount::class, 'driver_id');
    }
    
    public function evaluations()
    {
    	return $this->hasMany(Evaluation::class, 'driver_id')->orderBy('start_date','desc');
    }
    
    public function active_evaluations()
    {
    	return $this->hasMany(Evaluation::class, 'driver_id')->where('status','=',1)->orderBy('start_date','desc');
    }

    public function vehicle()
    {
        return $this->hasOne(Vehicle::class, 'fk_user_id')->latest('id');
    }
    
    public function campaign_city()
    {
    	return $this->belongsTo(CampaignCity::class, 'city_id');
    }
    
    public function bank_account()
    {
    	return $this->hasOne(BankAccount::class, 'fk_user_id');
    }
    
    public function getPrevNextDriver(Campaign $campaign)
    {
        $prev = CampaignDriver::with('driver')->where('campaign_id', $campaign->id)->where('driver_id', '<', $this->id)->orderBy('driver_id', 'DESC')->first();
        $next = CampaignDriver::with('driver')->where('campaign_id', $campaign->id)->where('driver_id', '>', $this->id)->orderBy('driver_id', 'ASC')->first();

        $result = new \StdClass;
        $result->prevDriver = $prev ? $prev->driver : null;
        $result->nextDriver = $next ? $next->driver : null;

        return $result;
    }
    
    public function getLatestCampaignAttribute()
    {        
        foreach($this->latest_campaign_requests as $cr){
           if($cr->status == 'approved'){
               return $cr->campaign;
           }
        }
        return $this->latest_campaign_requests[0]->campaign;
    }
    
    public function getReferralCodeAttribute()
    {        
        return empty($this->campaign_city) ? '' : $this->campaign_city->abbr . str_pad($this->getOriginal('referral_code'), 5, '0', STR_PAD_LEFT);
    }
    
    public function getLatestCampaignRequestAttribute()
    {
        foreach($this->latest_campaign_requests as $cr){
           if($cr->status == 'approved'){
               return $cr;
           }
        }
        return $this->latest_campaign_requests[0];
    }
    
    //helper
    public function setBlockedBecauseAttribute($reason_id){
        $this->is_active = 0;
        $this->save();
        $reason = ReasonBlock::find($reason_id);
        \DB::table('stickearn_v2.history_block')->insert([
            'driver_id'=>$this->id,
            'datetime'=>date('Y-m-d H:i:s'),
            'reason_id'=>$reason->id,
            'reason_text'=>$reason->text
        ]);
    }
    
    public function setAssignToAttribute($value){
        
        //get values
        $campaign_id = $value['campaign_id'];
        $odo_number = @$value['odo_number'] ? $value['odo_number'] : 0; //optional
        
        //get associated CRs
        $id = $this->id;
        $cr_exist = \App\Model\CampaignRequest::where('driver_id','=',$id)->where('campaign_id','=',$campaign_id)->orderBy('id','desc')->first();
        
        //prevent duplciate assign
        $opts = $value;
        $opts['comment'] = "assigned to campaign $campaign_id";
        $opts['campaign_id'] = $campaign_id;
        $this->setPreventDuplicateAssignAtrribute($opts);
        
        //check if reassignment
        if(!empty($cr_exist)){
            $cr_exist->reassign = $value;
        }else{
            //new assignment
            $campaign = \App\Model\Campaign::find($campaign_id);
            $data = [
                'campaign_id'=>$campaign_id, 
                'driver_id'=>$id,
                'status'=>'approved',
                'created_date'=>date('Y-m-d H:i:s'),
                'updated_date'=>date('Y-m-d H:i:s'),
                'wrap_type'=>$campaign->wrap_type_id,
                'wrap_start_date'=>date('Y-m-d'),
                'wrap_end_date'=>$campaign->end_date,
                'odo_number'=>$odo_number,
                'odo_sticker'=>@$this->vehicle->odo_sticker ? $this->vehicle->odo_sticker : 0,
                'vehicle_id'=>@$this->vehicle->id ? $this->vehicle->id : 0
            ];
            $cr = new \App\Model\CampaignRequest($data);
            $cr->save();

            //create evals
            \App\Events::driverAssigned($campaign_id,$id);

            //log
            $opts = $value;
            Events::campaignRequestSet($cr,'assign',$value);
        }
        
    }
    
    public function setAssignToReplaceAttribute($value){
        
        //get values
        $campaign_id = $value['campaign_id'];
        $replacee_id = $value['replacee_id'];
        $odo_number = @$value['odo_number'] ? $value['odo_number'] : 0; //optional
        
        //get associated CRs
        $id = $this->id;
        $cr_exist = \App\Model\CampaignRequest::where('driver_id','=',$id)->where('campaign_id','=',$campaign_id)->orderBy('id','desc')->first();

        //prevent duplciate assign
        $opts = $value;
        $opts['comment'] = "assigned to campaign $campaign_id";
        $opts['campaign_id'] = $campaign_id;
        $this->setPreventDuplicateAssignAtrribute($opts);
        
        //update replacee campaign cr
        $campaign = Campaign::find($campaign_id);
        $replacee_cr = \App\Model\CampaignRequest::where('driver_id','=',$replacee_id)->where('campaign_id','=',$campaign->id)->orderBy('id','desc')->first();
        $replacee_cr->status = 'suspended';
        $replacee_cr->replacer = $this->id;
        $replacee_cr->save();

        //log
        $opts = [];
        $opts['comment'] = "replaced by campaign_request driver_id $id";
        if(@$value['admin_id']){ $opts['admin_id'] = $value['admin_id']; }
        if(@$value['model']){ $opts['model'] = $value['model']; }
        if(@$value['created_from']){ $opts['created_from'] = $value['created_from']; }
        Events::campaignRequestSet($replacee_cr,'suspend',$opts);
        
        //update replacer id for all replacee's ancestors
        \DB::table('campaign_request')
                ->where('campaign_id','=',$campaign->id)
                ->where('replacer','=',$replacee_id)
                ->update([
                    'replacer'=>$this->id
                ]);
        
        //check if reassignment
        if(!empty($cr_exist)){
            $cr_exist->reassign = $value;
        }else{
            //new assignment
            $campaign = \App\Model\Campaign::find($campaign_id);
            $replace_cr = new \App\Model\CampaignRequest([
                'campaign_id'=>$campaign->id,
                'driver_id'=>$this->id,
                'wrap_type'=>$campaign->wrap_type_id,
                'status'=>'approved',
                'created_date'=>date('Y-m-d H:i:s'),
                'updated_date'=>date('Y-m-d H:i:s'),
                'wrap_start_date'=>$campaign->start_date,
                'wrap_end_date'=>$campaign->end_date,
                'odo_number'=>$odo_number,
                'odo_sticker'=>@$this->vehicle->odo_sticker ? $this->vehicle->odo_sticker : 0,
                'vehicle_id'=>@$this->vehicle->id ? $this->vehicle->id : 0
            ]);
            $replace_cr->save();

            //create evals
            \App\Events::driverAssigned($campaign_id,$id);

            //log
            $opts = $value;
            Events::campaignRequestSet($replace_cr,'assign',$value);
        }
    }
    
    public function setOptionalParamsAttribute($value){
        if(!empty($value['referral_code'])){
            $this->grab_referral_code = $value['referral_code'];
            $this->save();
        }
        if(!empty($value['grab_status'])){
            $this->grab_status = $value['grab_status'];
            $this->save();
        }
        if(!empty($value['odo_sticker'])){
            $this->vehicle->odo_sticker = $value['odo_sticker'];
            $this->vehicle->save();
        }
        
        if(!empty($value['account_number'])){
            $prev_bank_id = 0;
            $prev_account_number = 0;
            if(!empty($this->bank_account)){
                $prev_bank_id = $this->bank_account->bank_id;
                $prev_account_number = $this->bank_account->account_number;
            }
            
            $bank_id = 0;
            $bank_name = '';
            $holder_name = $this->first_name.' '.$this->last_name;
            if(!empty($value['bank_id'])){
                $bank = \App\Model\Bank::find($value['bank_id']);
                $bank_id = $bank->id;
                $bank_name = $bank->bank_name;
            }
            if (!empty($value['holder_name'])) {
                $holder_name = $value['holder_name'];
            }
            
            if($prev_bank_id != $bank_id || $prev_account_number != $value['account_number']){
                \DB::table('stickearn_v2.history_bank_account')->insert([
                    'driver_id'=>$this->id,
                    'admin_id'=>Auth::guard('admin')->user()->id,
                    'prev_account_number'=>$prev_account_number,
                    'new_account_number'=>$value['account_number'],
                    'prev_bank_id'=>$prev_bank_id,
                    'new_bank_id'=>$bank_id,
                    'created_date'=>date('Y-m-d H:i:s'),
                    'updated_date'=>date('Y-m-d H:i:s'),
                ]);
            }
            
            $this->bank_account()->updateOrCreate(['fk_user_id' => $this->id],[
                'bank_name'=>'',
                'bank_id'=>$bank_id,
                'branch_code'=>$bank_name,
                'holder_name'=>$holder_name,
                'account_number'=> $value['account_number']
            ]);
        }
    }

    public function setPreventDuplicateAssignAtrribute($value){
        $last_crs = \App\Model\CampaignRequest::where('driver_id','=',$this->id)->where('status','=','approved');
        if(@$value['campaign_id']){
            $last_crs = $last_crs->where('campaign_id','!=',$value['campaign_id']);
        }
        $last_crs = $last_crs->orderBy('id','desc')->get();
        if(!empty($last_crs)){
            foreach($last_crs as $last_cr){
                $last_cr->complete = $value;
            }
        }
    }
}