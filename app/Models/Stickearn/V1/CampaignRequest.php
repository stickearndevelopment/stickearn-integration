<?php

namespace App\Models\Stickearn\V1;

use Illuminate\Database\Eloquent\Model;
use App\Events;

use Illuminate\Support\Facades\DB;

class CampaignRequest extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';

    protected $table = 'campaign_request';
    protected $guarded = [];

    public function campaign()
    {
    	return $this->belongsTo(Campaign::class, 'campaign_id');
    }
    
    public function driver()
    {
    	return $this->belongsTo(Driver::class, 'driver_id');
    }
    
    public function histories()
    {
    	return $this->hasMany(HistoryCampaignRequest::class, 'campaign_request_id');
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }
    
    public function setAddEvaluationAttribute($value)
    {
        if ( ! array_key_exists('campaign', $this->relations)) $this->load('campaign');
        if ( ! array_key_exists('campaign.addons', $this->relations)) $this->load('campaign.addons');
        $wrap_type_id = $this->campaign->wrap_type_id;
        $left_car = 'disabled';
        $right_car = 'disabled';
        $front_car = 'disabled';
        if(in_array($wrap_type_id, [3,4])){
            $left_car = 'enabled';
            $right_car = 'enabled';
        }else if(in_array($wrap_type_id, [5])){
            $left_car = 'enabled';
            $right_car = 'enabled';
            $front_car = 'enabled';
        }

        $eval_data = [
            'campaign_id'=>$this->campaign_id,
            'driver_id'=>$this->driver_id,
            'status'=>0,
            'km_odometer'=>0,
            'odometer'=>'enabled',
            'back_window'=>'enabled',
            'front_car'=>$front_car,
            'left_car'=>$left_car,
            'right_car'=>$right_car,
            'start_date'=>date($value['start_date']),
            'end_date'=>date($value['end_date']),
            'offline_km'=>0,
            'created_on'=>NULL,
            'created_by'=>1,
            'eval_num'=>$value['eval_num'],
            'upload_date'=>NULL
        ];
        
        $eval = new Evaluation($eval_data);
        $eval->save();
        
        //addons
        $addons_ids = [];
        foreach($this->campaign->addons as $addon){
            $addons_ids[] = $addon->id;
        }
        $eval->addons()->attach($addons_ids);
    }

    public function setReassignAttribute($value)
    {
        //get values
        $odo_number = 0;
        if(is_array($value)){
            $odo_number = @$value['odo_number'] ? $value['odo_number'] : 0;
        }
        $campaign_id = $this->campaign_id;

        //reassignment
        $this->status = 'approved';
        $this->odo_number = $odo_number;
        $this->updated_date = date('Y-m-d H:i:s');
        $this->save();

        //log
        $opts = $value;
        $opts['comment'] = "reassigned to campaign $campaign_id";
        Events::campaignRequestSet($this,'reassign',$opts);
    }

    public function setCompleteAttribute($value)
    {
        //set status
        $this->status = 'completed';
        $this->updated_date = date('Y-m-d H:i:s');
        $this->save();

        //log
        $opts = $value;
        Events::campaignRequestSet($this,'complete',$opts);
    }

    public function setSuspendAttribute($value)
    {
        //set status
        $this->status = 'suspended';
        $this->updated_date = date('Y-m-d H:i:s');
        $this->save();

        //log
        $opts = $value;
        Events::campaignRequestSet($this,'suspend',$opts);
    }

    public function setRejectAttribute($value)
    {
        //set status
        $this->status = 'rejected';
        $this->updated_date = date('Y-m-d H:i:s');
        $this->save();

        //log
        $opts = $value;
        Events::campaignRequestSet($this,'remove',$opts);
    }
    
    public function setGenerateEvaluationsAttribute($value){
        if ( ! array_key_exists('campaign', $this->relations)) $this->load('campaign');
        if ( ! array_key_exists('driver', $this->relations)) $this->load('driver');
        
        if($this->campaign->is_thirty_days > 0){
            if(!empty($this->campaign) && !empty($this->driver)){
            
                //get last eval
                $last_eval = Evaluation::where('campaign_id','=',$this->campaign->id)->where('driver_id','=',$this->driver->id)->orderBy('start_date','desc')->first();

                //create eval 0 if have no eval at all
                if(!@$last_eval){
                    $this->add_evaluation = [
                        'start_date'=>date('Y-m-d 00:00:00'),
                        'end_date'=>date('Y-m-d 23:59:59'),
                        'eval_num'=>0
                    ];
                }

                //create eval inbetween
                $date_i = date('Y-m-d 00:00:00',strtotime($this->campaign->start_date));
                $date_start = @$last_eval ? date('Y-m-d 00:00:00',strtotime($last_eval->start_date)) : date('Y-m-d 00:00:00');
                $eval_num_i = 1;
                $count = (@$last_eval ? $this->campaign->eval_count - $last_eval->eval_num : $this->campaign->eval_count);

//                $array = [];
                $first_eval_num = -1;
                while($count > 0 && ($first_eval_num < 5 || $eval_num_i <= $this->campaign->eval_count || $this->campaign->persistent_end_date > 0)){
//                    $count_now = $count;
                    if($date_i > $date_start){
                        if($first_eval_num == -1){
                            $first_eval_num = $eval_num_i;
                        }
                        if($first_eval_num >= 5 && $eval_num_i > $this->campaign->eval_count){
                            break;
                        }
                        $this->add_evaluation = [
                            'start_date'=>date('Y-m-d',strtotime($date_i)).' 00:00:00',
                            'end_date'=>date('Y-m-d',strtotime($date_i)).' 23:59:59',
                            'eval_num'=>$eval_num_i
                        ];
                    }
                    if($date_i >= $date_start){
                        $count--;
                    }
                    $date_i = date('Y-m-d 00:00:00', strtotime($date_i . " +".($eval_num_i == 1 ? '5' : '6')." days"));
                    $eval_num_i++;
//                    $count_late = $count;
//                    $array[] = [$count_now,$count_late,$date_i,$date_start];
                }

                //create last eval1
                if($this->campaign->must_remove > 0){
                    $end_date = $this->campaign->end_date;
                    $this->add_evaluation = [
                        'start_date'=>date('Y-m-d',strtotime($end_date)).' 00:00:00',
                        'end_date'=>date('Y-m-d',strtotime($end_date)).' 23:59:59',
                        'eval_num'=>$eval_num_i
                    ];
                }
            }
        }else{
            //create evaluations
            if(!empty($this->campaign)){
                $ev = Evaluation::where('campaign_id','=',$this->campaign->id)->where('driver_id','=',$this->driver->id)->first();
                if(empty($ev)){
                    if($this->campaign->persistent_end_date > 0){
                        DB::statement( "call stickearn_v2.sp_create_evaluation_3_persistent(:campaign_id,:driver_id);", ['campaign_id'=>$this->campaign->id, 'driver_id'=>$this->driver->id] );
                    }else{
                        DB::statement( "call stickearn_v2.sp_create_evaluation_3(:campaign_id,:driver_id);", ['campaign_id'=>$this->campaign->id, 'driver_id'=>$this->driver->id] );
                    }
                }
            }
        }
    }
}