<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Client extends Model implements JWTSubject, Authenticatable
{

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';

    protected $table = 'stickearn_v2.api_client';

    protected $guarded = [];

    protected $hidden = ['password'];

    protected $casts = [
        'is_active' => 'boolean',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Client $client) {
            $client->generateReferralCode();
        });
    }
    
    public function client_role()
    {
        return $this->belongsTo(ClientRole::class, 'role_id');
    }
    
    public function getRoleAttribute()
    {
        return $this->client_role->name ?? '';
    }

    public function getAuthIdentifierName()
    {
        return 'id';
    }

    public function getAuthIdentifier()
    {
        return $this->id;
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function getRememberToken()
    {
        return '';
    }

    public function setRememberToken($value)
    {
        return '';
    }

    public function getRememberTokenName()
    {
        return '';
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}