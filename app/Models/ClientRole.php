<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientRole extends Model
{
    protected $table = 'stickearn_v2.api_client_role';
}