<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof AccessDeniedHttpException) {
            return response('', 403);
        }

        if ($e instanceof AuthenticationException) {
            return response('Unauthorized', 401);
        }

        if ($e instanceof ValidationException) {
            $code = Response::HTTP_UNPROCESSABLE_ENTITY;

            return $this->respond($e->getMessage(), $code, $e->errors());
        }

        if ($e instanceof \ErrorException) {
            Log::error('ERROR', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);
        }

        if ($e instanceof QueryException) {
            Log::error('ERROR', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'query' => $e->getSql()
            ]);
        }
        
        if($e instanceof NotFoundHttpException)
        {
            return response('Not Found', 404);
        }
        
        if($e instanceof MethodNotAllowedHttpException)
        {
            return response('Not Found', 404);
        }
        
        if($e instanceof ModelNotFoundException)
        {
            return response('Not Found', 404);
        }

        return parent::render($request, $e);
    }

    protected function respond($message, $code, $data = null)
    {
        return response([
            'status' => false,
            'message' => $message,
            'http_code' => $code,
            'data' => $data
        ], $code);
    }
}
