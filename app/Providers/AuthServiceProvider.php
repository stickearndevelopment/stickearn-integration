<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $router = $this->app['router'];

        $router->post('oauth/token', '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');
    }
}
