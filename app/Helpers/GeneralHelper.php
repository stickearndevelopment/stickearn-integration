<?php

function isJSON($string){
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

function query_last_campaign_request_raw(){
    return "select * from (select * from stick_earn.campaign_request where id in (
            select max(id) from (
                    select * from stick_earn.campaign_request where (driver_id, campaign_id) in (
                    select latest.driver_id, ifnull(approved.campaign_id, latest.campaign_id)
                    from (
                            select * from stick_earn.campaign_request where id in (
                                    select max(id) from stick_earn.campaign_request where (driver_id, updated_date) in (
                                            select driver_id, max(updated_date) from stick_earn.campaign_request group by driver_id
                                    ) group by driver_id
                            )
                    ) latest
                    left join (
                            select * from stick_earn.campaign_request where id in (
                                    select max(id) from stick_earn.campaign_request where status = 'approved' group by driver_id
                            )
                    ) approved on approved.driver_id = latest.driver_id
            )
        ) a group by driver_id
    )) a";
}