<?php 
/**
 * Created by VSCode.
 * User: dedysetiawan
 * Date: 22/01/18
 * Time: 14.25
 */

namespace App\Repositories\Stickearn\V1;

use App\Models\Stickearn\V1\Driver;

class DriverRepository
{
    protected $driver;

    function __construct(Driver $driver)
    {
        $this->driver = $driver;
    }

    public function all()
    {
        return $this->$driver->all();
    }

    public function showAll($order, $limit, $skip, $where=[], $whereRaw=NULL)
    {
        $model = $this->driver
                    ->select('user_profile.*')
                    ->join('stick_earn.vehicle','vehicle.fk_user_id','=','user_profile.id')
                    ->with('vehicle.vehicle_type.vehicle_brand.vehicle_class','vehicle.vehicle_color','latest_campaign_requests');

        if ($whereRaw) {
            $model = $model->whereRaw($whereRaw);
        }

        foreach ($order as $key => $value){
            $model = $model->orderBy($key,$value);
        }
        
        foreach ($where as $key1 => $value1){
            $model = $model->where($key1,'=',$value1);
        }

        $driver = $model->take((int) $limit)
                        ->skip((int) $skip)
                        ->get()
                        ->all();
        
        $result = [
            'total' => $model->count(),
            'data' => $driver
        ];

        return $result;
    }

    public function find($id)
    {
        return $this->driver->find($id);
    }
    
    public function findOrFail($id)
    {
        return $this->driver->findOrFail($id);
    }

}