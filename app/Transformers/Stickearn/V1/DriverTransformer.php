<?php

namespace App\Transformers\Stickearn\V1;

use League\Fractal\TransformerAbstract;

use App\Transformers\SkeletonTransformer;
use App\Models\Stickearn\V1\Driver;

class DriverTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [];
    protected $availableIncludes = [];

    public function transform(Driver $driver)
    {
        return [
            'id'            => $driver->id,
            'name'          => $driver->first_name.' '.$driver->last_name,
            'mobile_number' => $driver->mobile_number,
            'email_id'      => $driver->email_id,
            'reg_number'    => @$driver->vehicle->reg_number ? $driver->vehicle->reg_number : '',
            'is_active'     => $driver->is_active,
            'vehicle_class' => @$driver->vehicle->vehicle_type->vehicle_brand->vehicle_class->name ? $driver->vehicle->vehicle_type->vehicle_brand->vehicle_class->name : '',
            'vehicle_brand' => @$driver->vehicle->vehicle_type->vehicle_brand->name ? $driver->vehicle->vehicle_type->vehicle_brand->name : '',
            'vehicle_type'  => @$driver->vehicle->vehicle_type->name ? $driver->vehicle->vehicle_type->name : '',
            'vehicle_color' => @$driver->vehicle->vehicle_color->name ? $driver->vehicle->vehicle_color->name : '',
            'campaign_status'       => @$driver->latest_campaign_request->status ? $driver->latest_campaign_request->status : '',
            'campaign_name'         => @$driver->latest_campaign->campaign_name ? $driver->latest_campaign->campaign_name : '',
            'campaign_start_date'   => @$driver->latest_campaign->start_date ? $driver->latest_campaign->start_date : '',
            'campaign_end_date'     => @$driver->latest_campaign->end_date ? $driver->latest_campaign->end_date : '',
        ];
    }
}
