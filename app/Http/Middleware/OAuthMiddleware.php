<?php

namespace App\Http\Middleware;

use Closure;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class OAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {
         $header = $request->headers->all();
         $stickheader = (isset($header['stickheader'][0]))?$header['stickheader'][0]:null;

         if ($stickheader) {
             $stickheader_decode = base64_decode($stickheader);
             if (isJSON($stickheader_decode) == False) {
                 return $this->errorHandle();
             }

             $stickheader_data = json_decode($stickheader_decode, true);
             if (empty($stickheader_data)) {
                 return $this->errorHandle();
             }

             if (array_key_exists("account",$stickheader_data)){
                 foreach ($stickheader_data as $key => $value) {
                     $request->request->add([$key => json_encode($value)]);
                 }
             } else {
                 return $this->errorHandle();
             }
         }

         return $next($request);
     }

     protected function errorHandle()
     {
         $response = [
             'status' => false,
             'http_code' => 401,
             'message' => "You don\'t have access for any private api with no account",
             'data' => null,
             'meta' => null
         ];

         return response()->json($response,'401',$headers = []);
     }
}
