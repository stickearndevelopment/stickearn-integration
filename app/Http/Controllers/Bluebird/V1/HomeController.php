<?php

namespace App\Http\Controllers\Bluebird\V1;

use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Transformers\SkeletonTransformer;

use App\Models\Bluebird\Driver as Driver;

class HomeController extends ApiController
{
    public function index(Request $request)
    {
        return 'ok';
    }
    
    public function distance(Request $request)
    {
        //validate
        $this->validate($request, [
            'date' => 'date_format:"Y-m-d"|required',
            'data' => 'array|required',
            'data.*.taxi_id' => 'string|required|distinct',
            'data.*.distance' => 'array|required',
            'data.*.distance.*' => 'integer|required',
        ]);
        
        //read and map datas
        $kabupatens = [];
        $tracking_ids = [];
        $inserts = [];
        foreach($request->data as $data){
            $tracking_ids[$data['taxi_id']] = '';
            foreach($data['distance'] as $area_name => $distance){
                $kabupatens[$area_name] = '';
                $inserts[] = (object)[
                    'tracking_id' => $data['taxi_id'],
                    'tanggal' => $request->date,
                    'kabupaten_map' => $area_name,
                    'distance' => $distance,
                    'impression' => $distance + rand(-40, 40)
                ];
            }
        }
        
        //get and map kabupaten
        $kabupaten_maps = [];
        $kab_maps = \DB::table('stickearn_v2.area_mapping')->whereIn('kabupaten_slug', array_keys($kabupatens))->whereRaw('kabupaten = kecamatan')->get();
        foreach($kab_maps as $map){
            $kabupaten_maps[$map->kabupaten_slug] = $map;
        }
        
        //get and map drivers
        $driver_maps = [];
        $drivers = Driver::selectRaw('user_profile.id, cr.campaign_id, cr.driver_id, user_profile.tracking_id')
                ->join(\DB::raw('('.query_last_campaign_request_raw().') cr'),'cr.driver_id','=','user_profile.id')
                ->whereIn('tracking_id', array_keys($tracking_ids))
                ->get();
        foreach($drivers as $driver){
            $driver_maps[$driver->tracking_id] = $driver;
        }
          
        //prepare inserts and deletes
        $bluebird_area_daily_inserts = [];
        $integration_distance_daily_inserts_array = [];
        $driver_distance_area_daily_inserts = [];
        $deletes1 = [];
        $deletes2 = [];
        foreach($inserts as $insert){
            if(@$driver_maps[$insert->tracking_id]){
                $insert = (object)$insert;
                $campaign_id = $driver_maps[$insert->tracking_id]->campaign_id;
                $driver_id = $driver_maps[$insert->tracking_id]->driver_id;
                $area_name = $kabupaten_maps[$insert->kabupaten_map]->kabupaten;
                $tanggal = $request->date;

                $deletes1[] = [$campaign_id,$driver_id,$tanggal,$area_name];

                $bluebird_area_daily_inserts[] = [
                    'campaign_id' => $campaign_id,
                    'driver_id' => $driver_id,
                    'tanggal' => $tanggal,
                    'area_name' => $area_name,
                    'distance' => $insert->distance,
                    'impression' => $insert->impression,
                ];

                $driver_distance_area_daily_inserts[] = [
                    'campaign_id' => $campaign_id,
                    'driver_id' => $driver_id,
                    'tanggal' => $tanggal,
                    'area_name' => $area_name,
                    'distance' => $insert->distance,
                    'impression' => $insert->impression,
                    'durasi' => 0
                ];

                if(!@$integration_distance_daily_inserts_array[$campaign_id.'_'.$driver_id.'_'.$tanggal]){
                    $integration_distance_daily_inserts_array[$campaign_id.'_'.$driver_id.'_'.$tanggal] = [
                        'campaign_id' => $campaign_id,
                        'driver_id' => $driver_id,
                        'tanggal' => $tanggal,
                        'api_client_role_id' => 2,
                        'distance' => $insert->distance,
                        'impression' => $insert->impression,
                    ];
                    $deletes2[] = [$campaign_id,$driver_id,$tanggal];
                }else{
                    $integration_distance_daily_inserts_array[$campaign_id.'_'.$driver_id.'_'.$tanggal]['distance'] += $insert->distance;
                    $integration_distance_daily_inserts_array[$campaign_id.'_'.$driver_id.'_'.$tanggal]['impression'] += $insert->impression;
                }
            }
        }
        $integration_distance_daily_inserts = [];
        foreach($integration_distance_daily_inserts_array as $key => $insert){
            $integration_distance_daily_inserts[] = $insert;
        }
        
        //delete existing
        if(@$deletes1){
            $columns1 = ['campaign_id','driver_id','tanggal','area_name'];
            $values1 = $this->whereInMultiple($deletes1);
            \DB::table('stickearn_v2.driver_distance_area_daily')->whereRaw('('.implode($columns1, ', ').') in ('.implode($values1, ', ').')')->delete();
            \DB::table('stickearn_v2.bluebird_area_daily')->whereRaw('('.implode($columns1, ', ').') in ('.implode($values1, ', ').')')->delete();
        }
        if(@$deletes2){
            $columns2 = ['campaign_id','driver_id','tanggal'];
            $values2 = $this->whereInMultiple($deletes2);
            \DB::table('stickearn_v2.integration_distance_daily')->whereRaw('('.implode($columns2, ', ').') in ('.implode($values2, ', ').')')->delete();
        }
        
//        \DB::enableQueryLog();
        \DB::table('stickearn_v2.driver_distance_area_daily')->insert($driver_distance_area_daily_inserts);
        \DB::table('stickearn_v2.bluebird_area_daily')->insert($bluebird_area_daily_inserts);
        \DB::table('stickearn_v2.integration_distance_daily')->insert($integration_distance_daily_inserts);
//        dd(\DB::getQueryLog());
        //return ok
        $return = $request->all();
        return $this->respondWithItem($return, new SkeletonTransformer());
    }
    
    private function whereInMultiple(array $values)
    {
        return array_map(function (array $value) {
            return "('".implode($value, "', '")."')"; 
        }, $values);
    }
}