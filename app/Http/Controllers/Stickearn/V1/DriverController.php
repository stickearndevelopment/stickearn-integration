<?php

namespace App\Http\Controllers\Stickearn\V1;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Transformers\Stickearn\V1\DriverTransformer;
use App\Repositories\Stickearn\V1\DriverRepository;

class DriverController extends ApiController
{
    protected $driverRepository;

    public function __construct(DriverRepository $driverRepository)
    {
        parent::__construct();

        $this->driverRepository = $driverRepository;
    }
    
    public function index(Request $request)
    {
        $validator = Validator::make($input = $request->all(), [
            'page' => 'integer',
            'limit' => 'integer',
            'search' => 'string',
        ]);
        if ($validator->fails()) {
            return $this->errorWrongArgs($validator->errors()->first());
        }

        $defaultPerPage = 10;

        $args = $request->all();
        $search = @$args['search'] ? $args['search'] : 1;
        $page = @$args['page'] ? $args['page'] : 1;
        $limit = @$args['limit'] ? $args['limit'] : $defaultPerPage;
        $skip = ($page - 1) * $limit;
        $order_by = @$args['order_by'] ? $args['order_by'] : 'id';
        $order_dir = @$args['order_dir'] ? $args['order_dir'] : 'desc';
        $order[$order_by] = $order_dir;

        $whereRaw = '';
        if(@$search){
            $whereRaw = "concat(user_profile.first_name,' ',user_profile.last_name) like '%$search%'
                        or user_profile.email_id like '%$search%'
                        or user_profile.mobile_number like '%$search%'
                        or vehicle.reg_number like '%$search%'";
        }

        $drivers = $this->driverRepository->showAll($order, $limit, $skip, [], $whereRaw);

        return $this->setMeta([
            'count' => (int) count($drivers['data']),
            'total' => (int) $drivers['total'],
            'current_page' => (int) $page,
            'max_page' => (int) ceil($drivers['total'] / $limit),
            'limit' => $limit,
        ])->respondWithCollection($drivers['data'], new DriverTransformer());
    }
    
    public function detail(Request $request, $driver_id)
    {
        $driver = $this->driverRepository->findOrFail($driver_id);

        return $this->setMeta([])->respondWithItem($driver, new DriverTransformer());
    }
}