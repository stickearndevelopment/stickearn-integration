<?php

namespace App\Http\Controllers\Stickearn\V1;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

class HomeController extends ApiController
{
    public function index(Request $request)
    {
        return 'ok';
    }
}