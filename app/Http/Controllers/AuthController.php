<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Models\Client;
use App\Transformers\SkeletonTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;

class AuthController extends ApiController
{

    public function login(Request $request)
    {
        $validator = Validator::make($input = $request->all(), [
            'email' => 'required|email',
            'password' => 'required|string',
        ]);
        if ($validator->fails()) {
            return $this->errorValidation($validator->errors()->first());
        }

        $email = $request->input('email');
        $password = $request->input('password');

        $user = Client::where([
            'email' => $email,
            'is_active' => true
        ])->first();

        if ($user && password_verify($password, $user->password)) {

            $token = app('auth')->login($user);

            $resource = [
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => app('auth')->factory()->getTTL() * 60,
            ];

            return $this->respondWithItem($resource, new SkeletonTransformer);
        }

        return $this->errorValidation(__('auth.wrong_credentials', [], 'id'));
    }

    public function logout()
    {
        app('auth')->logout();
        return $this->respondWithItem(['Succesfully logged out.'], new SkeletonTransformer);
    }

    public function refreshToken(Request $request)
    {
        JWTAuth::setToken($request->bearerToken());

        $token = JWTAuth::refresh(JWTAuth::getToken());

        JWTAuth::setToken($token);

        $client = JWTAuth::authenticate();

        return $this->respondWithItem([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => app('auth')->factory()->getTTL() * 60,
        ], new SkeletonTransformer());
    }
}
