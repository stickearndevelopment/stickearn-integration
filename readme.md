# Lumen Skeleton API for Development (StickEarn Group)

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](http://lumen.laravel.com/docs).

## Step to Start Development

1. Git clone this repository.
2. delete .git file and .git folder or run this command in terminal ``rm -rf mydir``
3. type in terminal ``git remote add origin {your_project_repository}``, then ``git push -u origin master`` (ref: http://kbroman.org/github_tutorial/pages/init.html).
4. rename .env.example to .env and set database configurations (read instruction in .env file) and application path to folder you work.
5. type command ``composer install`` in terminal.
6. Start your project development.

## Development Reference
- Lumen Using repository pattern : https://bosnadev.com/2015/03/07/using-repository-pattern-in-laravel-5/
- Output Response use Transformer league fractal : https://fractal.thephpleague.com/
- Carbon Nesbot for DateTime : http://carbon.nesbot.com/

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

Edited By Dedy Setiawan - 29/01/2018.
